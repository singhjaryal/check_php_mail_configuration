<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Php Mail Configuration</title>
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row my-4 mailCard">
            <div class="card col-lg-8 col-md-8 m-auto p-0">
                <div class="card-header text-center text-uppercase bg-info text-white">
                    <h2>Php Mail Configuration</h2>
                </div>
                <div class="card-body">
                    <form action="mailConfiguration.php" method="post">
                        <div class="form-group">
                            <label for="" class="form-label">Host Name</label>
                            <input type="text" class="form-control" name="hostname" placeholder="Enter Host Name" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Username</label>
                            <input type="email" class="form-control" name="username" placeholder="Enter Mail" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter Password" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Recipent Name</label>
                            <input type="text" class="form-control" name="recipentName" placeholder="Enter Recipent Name" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Recipent Mail</label>
                            <input type="email" class="form-control" name="recipentMail" placeholder="Enter Recipent Mail" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Msg Send for Mail</label>
                            <textarea class="form-control" name="msg" rows="4" placeholder="Enter Text...." required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Check" name="submit" class="btn btn-success mr-2">
                            <input type="submit" value="View" name="viewConfiguration" class="btn btn-info">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <footer class="bg-dark fixed-bottom p-3 text-white text-center">
        <small>Created By <i class="text-warning">SINGH JARYAL</i></small>
    </footer>



    <script src="assets/js/custom.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
</body>
</html>
