<?php session_start();
$data = $_SESSION['data'];
error_reporting(0); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Show PHP Mail Configuration</title>
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  </head>
  <body>
    <section class="container">
      <div class="card col-lg-6 col-md-6 show-mail">
        <div class="card-header">
          PHP Mail Configuration
        </div>
        <div class="card-body">
          <div class="lib">
            <a href="https://github.com/PHPMailer/PHPMailer/tree/5.2-stable">PHPMailer Liberary</a>
          </div>
          <div class="form-group">
            <label>HostName :-</label>
            <input type="text" value="<?php echo $data['hostname']; ?>">
          </div>
          <div class="form-group">
            <label>Username :-</label>
            <input type="text" value="<?php echo $data['username']; ?>">
          </div>
          <div class="form-group">
            <label>Password :-</label>
            <input type="text" value="<?php echo $data['password']; ?>">
          </div>
          <div class="form-group">
            <label>Port :-</label>
            <input type="text" value="<?php echo $data['port']; ?>">
          </div>
          <div class="form-group">
            <label>Encryption :-</label>
            <input type="text" value="<?php echo $data['encription']; ?>">
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
