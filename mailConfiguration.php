<?php
error_reporting(0);
session_start();
require './PhpMailerClasses/PHPMailerAutoload.php';

$hostname = $_POST['hostname'];
$username = $_POST['username'];
$password = $_POST['password'];
$recipentMail = $_POST['recipentMail'];
$name = $_POST['recipentName'];
$msg = $_POST['msg'];
if(isset($_POST['submit'])){

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = $hostname;  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;           // Enable SMTP authentication
    $mail->Username = $username;      // SMTP username
    $mail->Password = $password;      // SMTP password
    $mail->SMTPSecure = 'tls';        // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;
    $mail->setFrom($mail->username, 'Mail Configuration');
    $mail->addAddress($recipentMail, $name);     // Name is optional
    $mail->isHTML(true);
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->Subject = 'Contact Form';    // Set email format to HTML
    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
}elseif(isset($_POST['viewConfiguration'])){
  $port = 587;
  $encyption = 'tls';
    $data = [
      'hostname' => $hostname,
      'username' => $username,
      'password' => $password,
      'port' => $port,
      'encription' => $encyption
    ];
    header('location: showMailConfiguration.php?data='.$data);
}else{
    header("location:index.php");
}

?>
